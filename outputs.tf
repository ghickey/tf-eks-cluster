
output "name" {
  description = "Cluster name"
  value       = module.cluster.cluster_id
}

output "location" {
  description = "Google zone where the cluster is located"
  value       = var.region
}


# output "cluster_username" {
#   description = "Username for cluster administrator"
#   value       = google_container_cluster.primary.master_auth.0.username
# }

# output "cluster_password" {
#   description = "Password for cluster administrator"
#   value       = google_container_cluster.primary.master_auth.0.password
# }

output "cluster_ca_certificate" {
  description = "Base64 encoded public certificate that is the root of trust for the cluster"
  value       = module.cluster.cluster_certificate_authority_data
  sensitive   = true
}

output "endpoint" {
  description = "The IP address of this cluster's Kubernetes master"
  value       = module.cluster.cluster_endpoint
  sensitive   = true
}

# output "services_ipv4_cidr" {
#   description = "The IP address range of the Kubernetes services in this cluster, in CIDR notation"
#   value       = google_container_cluster.primary.services_ipv4_cidr
# }

# output "instance_group_urls" {
#   description = "List of instance group URLs which have been assigned to the cluster"
#   value       = google_container_cluster.primary.instance_group_urls
# }

output "master_version" {
  description = "The current version of the master in the cluster"
  value       = module.cluster.cluster_version
}

# output "owner" {
#   description = "Email address of the cluster owner"
#   value       = var.owner
# }

# output "environment_name" {
#   description = "The environment (dev, test, prod)"
#   value       = var.environment_name
# }

# output "department" {
#   description = "Department ID to which this resource belongs"
#   value       = var.department
# }

output "node_count" {
  description = "Number of nodes in the node pool"
  value       = var.node_count
}

output "node_type" {
  description = "Node pool instance type"
  value       = var.node_type
}

output "iam_account" {
  description = "IAM account used by nodes"
  value       = ""
}
