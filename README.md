# Terraform EKS Cluster Module
=============================
This Terraform module will instantiate a GKE control plane.

As of v1.0 of this module, the node pool module has been incorporated into
this module. This eliminates the need to instantiate another module and
make more `terraform` calls to just stand up a cluster. The node pool
module will continue to exist for cases where additional--but different--
node types need to be added to a cluster. At the moment this is the only
solid use case for maintaining a node pool module.

Inputs
------

As of v1.2.0 of this module, the `k8s_version` input is now specified as
a pattern rather than the full version with patch levels. This allows
more flexibility in setting the input variables for using this module.
Patterns should follow the `version_prefix` specification in the
[Terraform docs](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/container_engine_versions)
### Common Variables

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| department | Department ID to which this resource belongs | string | n/a | yes |
| driver | aws, google, azure, etc. | string | `"google"` | no |
| env\_repo | The Terragrunt or environment repo that this module was called from. | string | n/a | yes |
| environment\_name | The environment (dev, test, prod) | string | n/a | yes |
| nugget | Naming moniker for relating all deployed objects | string | `""` | no |
| product\_name | The overall product that will be deployed in this infrastructure | string | n/a | yes |
| project | Google project ID | string | n/a | yes |
| region | The region to deploy to (e.g. us-east-1) | string | n/a | yes |

### Module Variables

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| extra_tags | Additional tags to add to the cluster | map | `{}` | no |
| location | Google zone or region | string | `"us-central1-b"` | no |
| k8s_version | Kubernetes version pattern for cluster creation | `null` | no |
| enable_autopilot | Create an GKE autopilot cluster | `false` | no |
| name | Cluster name | string | n/a | yes |
| node\_count | Initial node count to create per zone | string | `"2"` | no |
| node\_preemptible | Set node pool to use preemptible nodes | boolean | `true` | no |
| node\_type | Google instance type | string | n/a | yes |
| oauth\_scopes | Additional OAuth scopes to apply to nodes. Logging and monitoring scopes are *always* present | list | n/a | yes |
| owner | Email address of the cluster owner | string | n/a | yes |

Outputs
-------

| Name | Description |
|------|-------------|
| cluster\_ca\_certificate | Base64 encoded public certificate that is the root of trust for the cluster |
| cluster\_password | Password for cluster administrator |
| cluster\_username | Username for cluster administrator |
| department | Department ID to which this resource belongs |
| endpoint | The IP address of this cluster's Kubernetes master |
| environment\_name | The environment (dev, test, prod) |
| instance\_group\_urls | List of instance group URLs which have been assigned to the cluster |
| k8s\_requested\_version | Kubernetes version requested when provisioning cluster |
| k8s\_installed\_version | Kubernetes version installed |
| location | Google zone where the cluster is located |
| master\_version | The current version of the master in the cluster |
| name | Cluster name |
| node\_count | Number of nodes in the node pool |
| node\_type | Node pool instance type |
| owner | Email address of the cluster owner |
| project | Google project ID |
| services\_ipv4\_cidr | The IP address range of the Kubernetes services in this cluster, in CIDR notation |
