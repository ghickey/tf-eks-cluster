terraform {
  required_version = ">= 0.14"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.20.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "2.1.0"
    }

    null = {
      source  = "hashicorp/null"
      version = "3.1.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.1"
    }
  }


  backend "http" {}
}

provider "aws" {
  region      = var.region
}


module "meta" {
  source = "git::ssh://git@gitlab.com/ghickey/tf-gl-meta.git?ref=v0.1.3"

  env_repo         = var.env_repo
  environment_name = var.environment_name
  project          = var.project

  customer_facing   = "false"
  department        = var.department
  owner             = var.owner
  tf_module         = "ghickey__tf-gke-cluster"
  tf_module_version = "v2_1_1"

  extra_tags = var.extra_tags
}
