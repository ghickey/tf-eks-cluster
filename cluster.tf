
data "aws_eks_cluster" "cluster" {
  name = module.cluster.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.cluster.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

module "cluster" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = var.name
  cluster_version = var.k8s_version
  subnets         = module.network.private_subnets

  vpc_id = module.network.vpc_id

  workers_group_defaults = {
    root_volume_type = "gp2"
  }

  worker_groups = [
    {
      name                          = "worker-group-1"
      instance_type                 = var.node_type
      additional_userdata           = "echo foo bar"
      asg_desired_capacity          = var.node_count
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]
    },
  ]
}

resource "null_resource" "create_kubectl_context" {
  provisioner "local-exec" {
    command = "aws eks --region ${var.region} update-kubeconfig --name ${var.name}"
  }

  depends_on = [module.cluster]
}
