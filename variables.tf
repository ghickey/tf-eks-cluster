variable "driver" {
  description = "aws, google, azure, etc."
  default = "google"
}

variable "name" {
  description = "Cluster name"
}

variable "location" {
  description = "Google zone or region"
  default     = "us-central1-b"
}

variable "extra_tags" {
  description = "Additional tags to add to the cluster"
  type        = map
  default     = {}
}

variable "owner" {
  description = "Email address of the cluster owner"
}

variable "k8s_version" {
  description = "Kubernetes version for cluster creation"
  default     = null
}

variable "release_channel" {
  description = "Release channel used to determine Kubernetes version to deploy"
  default     = null
}

variable "private_cluster" {
  description = "Create a cluster not available to the public internet"
  default     = false
}

variable "enable_autopilot" {
  description = "Create cluster as an autopilot cluster"
  default     = false
}

variable "node_type" {
  description = "Google instance type"
}

variable "node_count" {
  description = "Initial node count to create per zone"
  default     = "2"
}

variable "node_preemptible" {
  description = "Set node pool to use preemptible nodes"
  default     = true
}

variable "oauth_scopes" {
  description = "Additional OAuth scopes to apply to nodes. Logging and monitoring scopes are *always* present"
  type        = list
  default     = null
}
