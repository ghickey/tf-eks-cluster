
################################################################################
# Common Variables
################################################################################

variable "project" {
  description = "Google project ID"
}

variable "region" {
  description = "The region to deploy to (e.g. us-east-1)"
}

variable "environment_name" {
  description = "The environment (dev, test, prod)"
}

variable "department" {
  description = "Department ID to which this resource belongs"
}

variable "product_name" {
  description = "The overall product that will be deployed in this infrastructure"
}

variable "nugget" {
  description = "Naming moniker for relating all deployed objects"
  default     = ""
}

variable "env_repo" {
  description = "The Terragrunt or environment repo that this module was called from."
}

locals {
  default_nugget = "${var.environment_name}-${var.product_name}"
  nugget         = var.nugget != "" ? var.nugget : local.default_nugget
}
